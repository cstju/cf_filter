# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 15:55:24 2016

@author: 3dlabuser
"""


import numpy as np
import time
#import scipy as sp
train_path='/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.base'
test_path='/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.test'
pre_path='/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1_pre_CF.csv'
class  Item_based:
	def __init__(self,X):
		self.X=np.array(X)
		print "the input data size is ",self.X.shape
		self.movie_user={}
		self.user_movie={}
		self.ave=np.mean(self.X[:,2])#得到评分的全局平均值
		for i in range(self.X.shape[0]):
			uid=self.X[i][0]#读取用户id
			mid=self.X[i][1]#读取电影id
			rat=self.X[i][2]#读取评分
			'''
			setdefault(key,default)
			是在字典中按照key去查找对应的值，如果没有对应的值就返回default值
			并且会新创建key-default这个键值对，
			在self.movie_user.setdefault(mid,{})这个语句中
			由于一开始设置的self.movie_user={}是一个空字典
			所以按mid查找时没有对应的值，就会默认创建一个空字典{}
			这个空字典的key是mid,value是空字典{}
			'''
			self.movie_user.setdefault(mid,{})
			self.user_movie.setdefault(uid,{})
			#以mid为一级key，uid为二级key，rat是value得到一个用户-电影评分矩阵
			#字典movie_user和字典user_movie的区别是索引方式不同
			#moive_user是以movie进行索引，user_movie是以user进行索引
			self.movie_user[mid][uid]=rat
			self.user_movie[uid][mid]=rat
		self.similarity={}
		#print type(self.movie_user)#是字典
		#print type(self.movie_user.setdefault(1,{}))#是字典
		#print type(self.movie_user[1][1])#是字典
		#print '用户1的评分记录：',self.user_movie.setdefault(1,{})#是字典
		#print '用户2的评分记录：',self.user_movie.setdefault(2,{})#是字典
		#print self.movie_user[][1]
		#print type(self.user_movie[1])#是字典
		#print self.user_movie[1]#用户1的评分记录{1: 5, 2: 3, 3: 4, 4: 3, 5: 3, 。。。。。}
		pass
	def sim_cal(self,m1,m2):
		#由于self.similarity字典中还没有m1这个key，会创建新的键值对，键是m1，值是{}，方便写入
		self.similarity.setdefault(m1,{})
		self.similarity.setdefault(m2,{})
		self.movie_user.setdefault(m1,{})#从新查找一遍，如果没有m1对应的值，则设为空字典{}
		self.movie_user.setdefault(m2,{})
		self.similarity[m1].setdefault(m2,-1)#将m1为key对应的字典中以m2为key的初始值设为-1
		self.similarity[m2].setdefault(m1,-1)#将m2为key对应的字典中以m1为key的初始值设为-1

		if self.similarity[m1][m2]!=-1:
			return self.similarity[m1][m2]
		si={}
		for user in self.movie_user[m1]:#遍历所有对电影m1进行过评分的用户
			if user in self.movie_user[m2]:#判断这些用户是否也对电影m2进行过评分
				si[user]=1#如果是，则加入到字典si中，得到电影m1和电影m2的共同评分用户集
		n=len(si)#得到共同评分用户集的用户数
		if (n==0):
			self.similarity[m1][m2]=1
			self.similarity[m2][m1]=1
			return 1
		
		################计算皮尔逊相似度的一种形式################
		s1=np.array([self.movie_user[m1][u] for u in si])
		s2=np.array([self.movie_user[m2][u] for u in si])
		sum1=np.sum(s1)#得到电影m1的评分总值
		sum2=np.sum(s2)#得到电影m2的评分总值
		s1_ave=sum1/n
		s2_ave=sum2/n
		sum1Sq=np.sum((s1-s1_ave)**2)
		sum2Sq=np.sum((s2-s2_ave)**2)
		pSum=np.sum((s1-s1_ave)*(s2-s2_ave))
		den=np.sqrt(sum1Sq*sum2Sq)
		if den==0:
			self.similarity[m1][m2]=0
			self.similarity[m2][m1]=0
			return 0
		self.similarity[m1][m2]=pSum/den
		self.similarity[m2][m1]=pSum/den
		return pSum/den
		###############计算皮尔逊相似度的另一种形式##############
		'''
		s1=np.array([self.movie_user[m1][u] for u in si])
		s2=np.array([self.movie_user[m2][u] for u in si])
		sum1=np.sum(s1)
		sum2=np.sum(s2)
		sum1Sq=np.sum(s1**2)
		sum2Sq=np.sum(s2**2)
		pSum=np.sum(s1*s2)
		num=pSum-(sum1*sum2/n)
		den=np.sqrt((sum1Sq-sum1**2/n)*(sum2Sq-sum2**2/n))
		if den==0:
			self.similarity[m1][m2]=0
			self.similarity[m2][m1]=0
			return 0
		self.similarity[m1][m2]=num/den
		self.similarity[m2][m1]=num/den
		return num/den
		'''
	def pred(self,uid,mid):
		sim_accumulate=0.0
		rat_acc=0.0
		for item in self.user_movie[uid]:#从目标uid用户中提取所有已评分电影
			sim=self.sim_cal(item,mid)#比较目标电影mid与目标用户的已评分电影集的相似度
			#？？？？？？？？？这里设定的是绝对阈值，如何设定相对阈值？？？？？？？？
			if sim<0.1:continue#设定一个阈值，小于0.2的就默认不相关，不进行计算
			rat_acc+=sim*self.user_movie[uid][item]#相似度与目标用户的已有评分相乘，再累加
			sim_accumulate+=sim#相似度累加
		#print rat_acc,sim_accumulate
		#如果一部电影没有被其他用户评过分，则选择全部电影平均分作为该电影的预测评分
		if sim_accumulate==0: 
			return  self.ave
		return rat_acc/sim_accumulate#累加的评分除以累加的相似度，得到预测评分
	def test(self,test_X):
		startTime=time.clock()
		test_X=np.array(test_X)
		output=[]
		sums=0
		print "the test data size is ",test_X.shape
		result = file(pre_path,'w') 
		for i in range(test_X.shape[0]):
			pre=self.pred(test_X[i][0],test_X[i][1])#得到用户对电影的预测分数,并没有取整
			#将预测分数写入到文件中
			result.write(str(test_X[i][0])+'\t'+str(test_X[i][1])+'\t'+str(pre))
			result.write("\r\n")
			output.append(pre)
			#print pre,test_X[i][2]
			sums+=(pre-test_X[i][2])**2#计算预测评分和真实评分之间的平方差
		result.close()
		#print "the predict rate's size: ",len(output)
		rmse=np.sqrt(sums/test_X.shape[0])
		print '计算RMSE结束, RMSE值为: %s; 用时: %s 秒' % (rmse, time.clock() - startTime)
		#print "the rmse on test data is ",rmse
		return output
def read_data():
	startTime=time.clock()
	train=open(train_path).read().splitlines()
	test=open(test_path).read().splitlines()
	train_X=[]
	test_X=[]
	for line in train:
		#将每一行的评分记录以空格为间隔分为数组p
		p=line.split('	');
		#将数组p的前三个元素：用户 电影 评分作为一个数组加入到train_X中
		train_X.append([int(p[0]),int(p[1]),int(p[2])])
	for line in test:
		p=line.split('	');
		test_X.append([int(p[0]),int(p[1]),int(p[2])])
	use_time=time.clock()-startTime 
	print '数据加载成功! 用时: %d秒' % use_time
	return train_X,test_X
train_X,test_X=read_data()
print '训练集个数：',np.array(train_X).shape[0]
print '测试集个数：',np.array(test_X).shape[0]
a=Item_based(train_X)
a.test(test_X)