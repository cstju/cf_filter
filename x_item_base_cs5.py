# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 15:55:24 2016

@author: 3dlabuser
"""


import numpy as np
import time
import sys, random, math
from operator import itemgetter

#import scipy as sp
pre_path='/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1_pre_CF.csv'
class ItemBasedCF():
	def __init__(self):
		self.trainset = {}#初始化训练集
		self.testset = {}#初始化测试集

		self.n_sim_movie = 20#限定相似物品集的个数为20
		self.n_rec_movie = 10#限定推荐物品个数为10

		self.similarity={}
		self.movie_sim_mat = {}
		self.movie_popular = {}
		self.movie_count = 0

		self.movie_user={}
		self.user_movie={}
		print 'Similar movie number = %d' % self.n_sim_movie
		print 'Recommended movie number = %d' % self.n_rec_movie

		simfactor_count = 0

	@staticmethod#staticmethod具体用法见：https://www.zhihu.com/question/20021164
	def loadfile(filename):
		''' load a file, return a generator. '''
		fp = open(filename, 'r')
        #enumerate函数会将数组或列表组成一个索引序列，可以返回列数跟值
		for i, line in enumerate(fp):
			yield line.strip('\r\n')
			if i % 100000 == 0:
				print 'loading %s(%s)' % (filename, i)
		fp.close()
		print 'load %s succ' % filename	


	def generate_dataset(self, filename, pivot=0.7):#按照7：3的比例分配测试集和训练集
		''' load rating data and split it to training set and test set '''
		trainset_len = 0
		testset_len = 0
		for line in self.loadfile(filename):
			#######################user, movie, rating, _ = line.split(',')
			user, movie, rating, _ = line.split('::')
			#######################user, movie, rating, _ = line.split('\t')
			# split the data by pivot
			if (random.random() < pivot):#每次生成0-1之间的随机数
				self.trainset.setdefault(user, {})#以用户为索引增加
				self.trainset[user][movie] = int(rating)#生成训练集 
				trainset_len += 1
			else:
				self.testset.setdefault(user, {})
				self.testset[user][movie] = int(rating)#生成测试集
				testset_len += 1

		print 'split training set and test set succ'
		print 'train set = %s' % trainset_len
		print 'test set = %s' % testset_len

	def sim_cal(self,m1,m2):
		#由于self.similarity字典中还没有m1这个key，会创建新的键值对，键是m1，值是{}，方便写入
		self.similarity.setdefault(m1,{})
		self.similarity.setdefault(m2,{})
		self.movie_user.setdefault(m1,{})#从新查找一遍，如果没有m1对应的值，则设为空字典{}
		self.movie_user.setdefault(m2,{})
		self.similarity[m1].setdefault(m2,-1)#将m1为key对应的字典中以m2为key的初始值设为-1
		self.similarity[m2].setdefault(m1,-1)#将m2为key对应的字典中以m1为key的初始值设为-1

		if self.similarity[m1][m2]!=-1:
			return self.similarity[m1][m2]
		si={}
		for user in self.movie_user[m1]:#遍历所有对电影m1进行过评分的用户
			if user in self.movie_user[m2]:#判断这些用户是否也对电影m2进行过评分
				si[user]=1#如果是，则加入到字典si中，得到电影m1和电影m2的共同评分用户集
		n=len(si)#得到共同评分用户集的用户数
		if (n==0):
			self.similarity[m1][m2]=1
			self.similarity[m2][m1]=1
			return 1
		
		################计算皮尔逊相似度的一种形式################
		s1=np.array([self.movie_user[m1][u] for u in si])
		s2=np.array([self.movie_user[m2][u] for u in si])
		sum1=np.sum(s1)#得到电影m1的评分总值
		sum2=np.sum(s2)#得到电影m2的评分总值
		s1_ave=sum1/n
		s2_ave=sum2/n
		sum1Sq=np.sum((s1-s1_ave)**2)
		sum2Sq=np.sum((s2-s2_ave)**2)
		pSum=np.sum((s1-s1_ave)*(s2-s2_ave))
		den=np.sqrt(sum1Sq*sum2Sq)
		if den==0:
			self.similarity[m1][m2]=0
			self.similarity[m2][m1]=0
			return 0
		self.similarity[m1][m2]=pSum/den
		self.similarity[m2][m1]=pSum/den	

	def calc_movie_sim_matix(self):
		
		print 'counting movies number and popularity...'
		for user, movies in self.trainset.iteritems():
			for movie in movies:
				# count item popularity 
				#这一段的目的是为了计算电影的流行度，统计每一部电影有多少人看过
				if movie not in self.movie_popular:
				#如果新输入的电影movie没有在movie_popular这个字典中
				#就将这个以movie为索引，字典对应的值设为0
					self.movie_popular[movie] = 0
				#movie已经在字典中，则以movie为索引对应的值加一
				self.movie_popular[movie] += 1

		print 'count movies number and popularity succ'

		# save the total number of movies
		self.movie_count = len(self.movie_popular)#统计完电影流行度后，所有的电影数也统计完毕
		print 'total movie number = %d' % self.movie_count

		# count co-rated users between items
		itemsim_mat = self.movie_sim_mat#建立电影相似矩阵
		print 'building co-rated users matrix...'
		#以下这段是统计两个电影共同被同一用户评分的次数，次数越多默认相似度越高（但是没有考虑评分的值啊）
		for m1,popular1 in self.movie_popular.iteritems():
			for m2,popular2 in self.movie_popular.iteritems():
				if m1 == m2: continue#如果电影1就是电影2，则跳过
				itemsim_mat.setdefault(m1,{})#不然就建立一个字典索引
				#itemsim_mat.setdefault(m2,{})#不然就建立一个字典索引
				itemsim_mat[m1].setdefault(m2,0)#设置m1,m2相似度，如果还没有m1m2索引，则建立初始相似度为0
				#itemsim_mat[m2].setdefault(m1,0)#设置m1,m2相似度，如果还没有m1m2索引，则建立初始相似度为0
				#if itemsim_mat[m1][m2] == 0 and itemsim_mat[m2][m1] == 0:
				itemsim_mat[m1][m2] = self.sim_cal(m1,m2)
					#itemsim_mat[m2][m1] = self.sim_cal(m1,m2)
		print 'calculate movie similarity matrix(similarity factor) succ'
		
	def pred(self,uid,mid):
		sim_accumulate=0.0
		rat_acc=0.0
		K = self.n_sim_movie
		for item in self.user_movie[uid]:#从目标uid用户中提取所有已评分电影
			for related_movie, sim in sorted(self.movie_sim_mat[mid].items(),
                    key=itemgetter(1), reverse=True)[:K]: 
				sim=self.movie_sim_mat[related_movie][mid]#比较目标电影mid与目标用户的已评分电影集的相似度
				if sim<0:continue
				#print sim,self.user_movie[uid][item],sim*self.user_movie[uid][item]
				rat_acc+=sim*self.user_movie[uid][item]#相似度与目标用户的已有评分相乘，再累加
				sim_accumulate+=sim#相似度累加
		#print rat_acc,sim_accumulate
		#如果一部电影没有被其他用户评过分，则选择全部电影平均分作为该电影的预测评分
		if sim_accumulate==0: 
			return  self.ave
		return rat_acc/sim_accumulate#累加的评分除以累加的相似度，得到预测评分
	def evaluate(self):
		
		for i in range(test_X.shape[0]):
			pre=self.pred(test_X[i][0],test_X[i][1])#得到用户对电影的预测分数,并没有取整
			#将预测分数写入到文件中
			#print pre,test_X[i][2]
			sums+=(pre-test_X[i][2])**2#计算预测评分和真实评分之间的平方差
		result.close()
		print 'write pre_rate_file succ'
		#print "the predict rate's size: ",len(output)
		rmse=np.sqrt(sums/test_X.shape[0])
		print '计算RMSE结束, RMSE值为: %s; 用时: %s 秒' % (rmse, time.clock() - startTime)
		#print "the rmse on test data is ",rmse
		return output
if __name__ == '__main__':
	ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 1M Dataset/ml-1m/ratings.dat'
	#################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 20M Dataset/ml-20m/ratings.csv'
	#################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.base'
	itemcf = ItemBasedCF()
	itemcf.generate_dataset(ratingfile)
	itemcf.calc_movie_sim_matix()
	print itemcf.pred(1,1193)
	#itemcf.test(prefile)