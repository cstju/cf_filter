# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 19:08:11 2016

@author: 3dlabuser
"""

import sys, random, math
from operator import itemgetter


random.seed(0)


class ItemBasedCF():
    ''' TopN recommendation - ItemBasedCF '''
    def __init__(self):
        self.trainset = {}#初始化训练集
        self.testset = {}#初始化测试集

        self.n_sim_movie = 20#限定相似物品集的个数为20
        self.n_rec_movie = 10#限定推荐物品个数为10

        self.movie_sim_mat = {}
        self.movie_popular = {}
        self.movie_count = 0

        print 'Similar movie number = %d' % self.n_sim_movie
        print 'Recommended movie number = %d' % self.n_rec_movie


    @staticmethod#staticmethod具体用法见：https://www.zhihu.com/question/20021164
    def loadfile(filename):
        ''' load a file, return a generator. '''
        fp = open(filename, 'r')
        #enumerate函数会将数组或列表组成一个索引序列，可以返回列数跟值
        for i, line in enumerate(fp):
            yield line.strip('\r\n')
            if i % 100000 == 0:
                print 'loading %s(%s)' % (filename, i)
        fp.close()
        print 'load %s succ' % filename


    def generate_dataset(self, filename, pivot=0.7):#按照7：3的比例分配测试集和训练集
        ''' load rating data and split it to training set and test set '''
        trainset_len = 0
        testset_len = 0

        for line in self.loadfile(filename):
            #######################user, movie, rating, _ = line.split(',')
            #######################user, movie, rating, _ = line.split('::')
            user, movie, rating, _ = line.split('\t')
            # split the data by pivot
            if (random.random() < pivot):#每次生成0-1之间的随机数
                self.trainset.setdefault(user, {})#以用户为索引增加
                self.trainset[user][movie] = int(rating)#生成训练集 
                trainset_len += 1
            else:
                self.testset.setdefault(user, {})
                self.testset[user][movie] = int(rating)#生成测试集
                testset_len += 1

        print 'split training set and test set succ'
        print 'train set = %s' % trainset_len
        print 'test set = %s' % testset_len


    def calc_movie_sim(self):
        ''' calculate movie similarity matrix '''
        print 'counting movies number and popularity...'
        '''
        iteritems()是字典操作方法，在需要迭代结果的时候使用最适合，而且它的工作效率非常的高。
        x = {'title':'python web site','url':'www.iplaypython.com'}
        for user, movies in x.iteritems():
            print user
            print movies
        输出结果是：
        url
        www.iplaypython.com
        title
        python web site
        '''
        for user, movies in self.trainset.iteritems():
            for movie in movies:
                # count item popularity 
                #这一段的目的是为了计算电影的流行度，统计每一部电影有多少人看过
                if movie not in self.movie_popular:
                    #如果新输入的电影movie没有在movie_popular这个字典中
                    #就将这个以movie为索引，字典对应的值设为0
                    self.movie_popular[movie] = 0
                #movie已经在字典中，则以movie为索引对应的值加一
                self.movie_popular[movie] += 1

        print 'count movies number and popularity succ'

        # save the total number of movies
        self.movie_count = len(self.movie_popular)#统计完电影流行度后，所有的电影数也统计完毕
        print 'total movie number = %d' % self.movie_count

        # count co-rated users between items
        itemsim_mat = self.movie_sim_mat#建立电影相似矩阵
        print 'building co-rated users matrix...'
        #以下这段是统计两个电影共同被同一用户评分的次数，次数越多默认相似度越高（但是没有考虑评分的值啊）
        for user, movies in self.trainset.iteritems():
            for m1 in movies:
                for m2 in movies:
                    if m1 == m2: continue#如果电影1就是电影2，则跳过
                    itemsim_mat.setdefault(m1,{})#不然就建立一个字典索引
                    itemsim_mat[m1].setdefault(m2,0)#设置m1,m2相似度，如果还没有m1m2索引，则建立初始相似度为0
                    itemsim_mat[m1][m2] += 1#每次出现都加1

        print 'build co-rated users matrix succ'

        # calculate similarity matrix 
        print 'calculating movie similarity matrix...'
        simfactor_count = 0
        PRINT_STEP = 2000000

        for m1, related_movies in itemsim_mat.iteritems():#先以m1为索引，得到所有与m1有关电影m2
            for m2, count in related_movies.iteritems():#在从与m1有关的所有m2电影中得到共同出现次数count
                itemsim_mat[m1][m2] = count / math.sqrt(#将m1与m2的相似度从新定义为共同出现次数/(各自的流行度的乘积再开平方)
                        self.movie_popular[m1] * self.movie_popular[m2])
                simfactor_count += 1#统计共有多少个相似度分数
                if simfactor_count % PRINT_STEP == 0:#每得到二十万个分数就输出一次
                    print 'calculating movie similarity factor(%d)' % simfactor_count

        print 'calculate movie similarity matrix(similarity factor) succ'
        print 'Total similarity factor number = %d' %simfactor_count


    def recommend(self, user):#推荐的输入只有用户编号，推荐结果是该用户编号想要看的电影编号
        ''' Find K similar movies and recommend N movies. '''
        K = self.n_sim_movie
        N = self.n_rec_movie
        rank = {}
        watched_movies = self.trainset[user]

        for movie,rate in watched_movies.items():
            for related_movie, w in sorted(self.movie_sim_mat[movie].items(),
                    key=itemgetter(1), reverse=True)[:K]:#取出电影相似矩阵中与已看过电影相似度前K个电影
                if related_movie in watched_movies:
                    continue
                rank.setdefault(related_movie, 0)
                rank[related_movie] += w#累加相似度
        # return the N best movies
        return sorted(rank.items(), key=itemgetter(1), reverse=True)[:N]#根据相似度排名返回前N个电影


    def evaluate(self):
        ''' return precision, recall, coverage and popularity '''
        print 'Evaluation start...'

        N = self.n_rec_movie#要推荐的电影的个数
        #  varables for precision and recall 
        hit = 0
        rec_count = 0
        test_count = 0
        # varables for coverage
        #Python中的set函数是一个无序不重复的元素集，是一个集合
        all_rec_movies = set()#统计一共有多少部电影被推荐
        # varables for popularity
        popular_sum = 0

        for i, user in enumerate(self.trainset):#先根据训练集检索出用户编号
            if i % 500 == 0:
                print 'recommended for %d users' % i
            test_movies = self.testset.get(user, {})#根据用户编号，得到在测试集中，该用户所看过的全部电影
            rec_movies = self.recommend(user)#根据用户编号，计算出为该用户推荐的电影集
            for movie, w in rec_movies:
                if movie in test_movies:
                    hit += 1
                all_rec_movies.add(movie)
                popular_sum += math.log(1 + self.movie_popular[movie])
            rec_count += N
            test_count += len(test_movies)

        precision = hit / (1.0 * rec_count)
        recall = hit / (1.0 * test_count)
        coverage = len(all_rec_movies) / (1.0 * self.movie_count)#覆盖率：被推荐的电影的总数量除以电影的总数量
        popularity = popular_sum / (1.0 * rec_count)

        print 'precision=%.4f\trecall=%.4f\tcoverage=%.4f\tpopularity=%.4f' \
                % (precision, recall, coverage, popularity)


if __name__ == '__main__':
    #################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 1M Dataset/ml-1m/ratings.dat'
    #################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 20M Dataset/ml-20m/ratings.csv'
    ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.base'
    itemcf = ItemBasedCF()
    itemcf.generate_dataset(ratingfile)
    itemcf.calc_movie_sim()
    itemcf.evaluate()
